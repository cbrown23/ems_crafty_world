package EMS_CRAFTY_world.visualisation;

import java.awt.Color;
import org.volante.abm.visualisation.*;

public class EMS_Agents extends AgentTypeDisplay
{	
	public EMS_Agents()
	{
		addAgent("High_Cereals", Color.yellow.darker());
		addAgent("Low_Cereals", Color.yellow.brighter());
		addAgent("High_Livestock", Color.blue.darker());
		addAgent("Low_Livestock", Color.blue.brighter());
		addAgent("Forester", Color.green.darker());
		}
}
